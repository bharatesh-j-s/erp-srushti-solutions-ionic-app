import { Injectable } from '@angular/core';
import { LoadingController, ToastController } from 'ionic-angular';
import { Toast } from '@ionic-native/toast';

@Injectable()
export class SpinnerProvider {
  
	loader: any;

	user = { name: '', username: '', email: '', mobile_no: '', avatar: '' };

	constructor(
		public loadingCtrl: LoadingController, 
		public toastCtrl: ToastController,
		private toast: Toast
	){
		console.log('Hello SpinnerProvider Provider');
	}

	load() {
		this.loader = this.loadingCtrl.create({
			spinner : 'crescent',
			content: 'Please wait...'
		});
		this.loader.present();
	}

	loadMesssage(message) {
		this.loader = this.loadingCtrl.create({
			spinner : 'crescent',
			content: message
		});
		this.loader.present();
	}

	dismiss() {
		if(this.loader){ 
			this.loader.dismiss(); 
			this.loader = null; 
		}
	}

	// message(message) {
	// 	let toast = this.toastCtrl.create({
	// 		message: message,
	// 		duration: 3000,
	// 		position: 'top'
	//   	});
	//   	toast.present();
	// }

	message(message) {
		this.toast.show(message, '3000', 'bottom').subscribe(
		toast => {
			console.log(toast);
		});
	}

}
