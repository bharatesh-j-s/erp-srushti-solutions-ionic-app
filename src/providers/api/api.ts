import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ApiProvider {

	apiUrl = 'https://srushtisolutions.in/api/v1/';
    locationUrl = 'https://srushtisolutions.in/api/v1/location/';
    changePasswordUrl = 'https://srushtisolutions.in/#/profile';
    userUrl = "https://srushtisolutions.in/storage/users/";
    productUrl = "https://srushtisolutions.in/storage/products/";
    imageUrl = "https://srushtisolutions.in/storage/images/";

    // apiUrl = 'http://159.65.159.184/testing/srushti/public/api/v1/';
    // locationUrl = 'http://159.65.159.184/testing/srushti/public/api/v1/location/';
    // changePasswordUrl = 'http://159.65.159.184/testing/srushti/public/#/profile';
    // userUrl = "http://159.65.159.184/testing/srushti/public/storage/users/";
    // productUrl = "http://159.65.159.184/testing/srushti/public/storage/products/";
    // imageUrl = "http://159.65.159.184/testing/srushti/public/storage/images/";

	constructor(public http: HttpClient) {
	    console.log('Hello ApiProvider Provider');
    }
    
	//Authentication
    login(data) {
        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'login', data)
            .subscribe(response => {
                resolve(response);
            }, 
            (error) => {
                reject(error);
            });
        });
    }

    checkVersion(data) {
        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'check_version', data)
            .subscribe(response => {
                resolve(response);
            }, 
            (error) => {
                reject(error);
            });
        });
    }

    savePlayer(data) {
        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'save_player', data)
            .subscribe(response => {
                resolve(response);
            }, 
            (error) => {
                reject(error);
            });
        });
    }

    getData(url,token,data) {
        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+url, data, {
                headers: new HttpHeaders().set('Authorization', token.token_type + ' ' + token.access_token)
            })
            .subscribe(response => {
                resolve(response);
            }, 
            (error) => {
                reject(error);
            });
        });
    } 

    postData(token,data) {
        return new Promise((resolve, reject) => {
            this.http.post(this.apiUrl+'get_branch', data, {
                headers: new HttpHeaders().set('Authorization', token.token_type + ' ' + token.access_token)
             })
            .subscribe(response => {
                resolve(response);
            }, 
            (error) => {
                reject(error);
            });
        });
    } 
}
