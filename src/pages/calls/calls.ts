import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, ModalController } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import { SpinnerProvider } from '../../providers/spinner/spinner';
import { ApiProvider } from '../../providers/api/api';

import { FilterCallsPage } from '../filter-calls/filter-calls';
import { ViewCallPage } from '../view-call/view-call';
import { AttendancePage } from '../attendance/attendance';

@Component({
	selector: 'page-calls',
	templateUrl: 'calls.html',
})
export class CallsPage {

	filter= { user_id:'', role:'', call_type_id:'', from_date:'', to_date:'', category_id:'', status_id:''};
	token= { access_token: '', token_type : ''};
	call_type: any;
	status: any;
	calls: any;

	constructor(
		public navCtrl: NavController, 
		public navParams: NavParams,
		public menu: MenuController,
		public modalCtrl : ModalController,
		public storage: Storage,
		public apiProvider: ApiProvider,
		public spinner: SpinnerProvider,
	){
		this.menu = menu;
		this.menu.enable(true, 'menu');
		this.call_type = navParams.get('data');
		this.status = navParams.get('data1');
		this.filter.call_type_id = this.call_type.call_type_id;
		if(this.status) {
			this.filter.status_id = this.status.status_id;
		}
		this.getUser();
	}

	getUser() {
		this.storage.get('user').then((data) => {
			if(data != null){
				this.filter.user_id = data.user.user_id;
				this.filter.role = data.user.role;
				this.token.access_token = data.access_token;
				this.token.token_type = data.token_type;
				this.getAttendanceStatus();
			}
		});
	}

	getAttendanceStatus() {
		this.apiProvider.getData('get_attendance_status',this.token,this.filter).then((response) => {
			this.status = response;
			if(this.status==0 && this.filter.role!='Super Admin') {
				this.navCtrl.push(AttendancePage);
			}
			else {
				this.getCalls();
			}
		}, 
		(error) => {
			this.spinner.message(error.error.message);
		});
	}

	getCalls() {
		this.spinner.load();
		this.apiProvider.getData('get_calls',this.token,this.filter).then((response) => {
			this.spinner.dismiss();
			this.calls = response;
		}, 
		(error) => {
			this.spinner.dismiss();
			this.spinner.message(error.error.message);
		});
	}

	filterCall() {
		let modal = this.modalCtrl.create(FilterCallsPage, { data: this.filter }, { cssClass:"filter-modal"});
		modal.onDidDismiss(data => {
			if(data!="close"){
				this.filter = data;
				this.getCalls();
			}
		});
		modal.present();
	}

	newCall() {

	}

	viewCall(call) {
		this.navCtrl.push(ViewCallPage, {
			data : call
		});
	}
}
