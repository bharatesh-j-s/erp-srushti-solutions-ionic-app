import { Component } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import { SpinnerProvider } from '../../providers/spinner/spinner';
import { ApiProvider } from '../../providers/api/api';

@Component({
  	selector: 'page-comments',
  	templateUrl: 'comments.html',
})
export class CommentsPage {

 	comment= { user_id:'', role:'', call_id:'', comment:'' }
	token= { access_token: '', token_type : ''};
	call: any;
	errors:any;

	constructor(
  		public navCtrl: NavController, 
		public navParams: NavParams,
		public menu: MenuController,
		public storage: Storage,
		public apiProvider: ApiProvider,
		public spinner: SpinnerProvider,
  	){
  		this.menu = menu;
        this.menu.enable(true, 'menu');
  		this.call = navParams.get('data');
  		this.comment.call_id = this.call.call_id;
  		this.getUser();
  	}

  	getUser() {
		this.storage.get('user').then((data) => {
			if(data != null){
				this.comment.user_id = data.user.user_id;
				this.comment.role = data.user.role;
				this.token.access_token = data.access_token;
				this.token.token_type = data.token_type;
				this.getCall();
			}
		});
	}

	getCall() {
		this.spinner.load();
		this.apiProvider.getData('get_call',this.token,this.comment).then((response) => {
			this.spinner.dismiss();
			this.call = response;
		}, 
		(error) => {
			this.spinner.dismiss();
			this.spinner.message(error.error.message);
		});
	}

	saveComment(){
		this.spinner.load();
		this.apiProvider.getData('save_comment',this.token,this.comment).then((response) => {
			this.spinner.dismiss();
			this.comment.comment="";
			this.getCall();
		}, 
		(error) => {
			this.spinner.dismiss();
			this.errors = error.error.errors;
			this.spinner.message(error.error.message);
		});
	}
}
