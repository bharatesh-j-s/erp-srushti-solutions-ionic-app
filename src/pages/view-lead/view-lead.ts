import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ModalController, ActionSheetController } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { SpinnerProvider } from '../../providers/spinner/spinner';
import { ApiProvider } from '../../providers/api/api';

import { CallNumber } from '@ionic-native/call-number';

import { LeadStatusPage } from '../lead-status/lead-status';
import { LeadCommentsPage } from '../lead-comments/lead-comments';
import { LeadReminderPage } from '../lead-reminder/lead-reminder';

import { Camera, CameraOptions } from '@ionic-native/camera';

import { Geolocation } from '@ionic-native/geolocation';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { LocationAccuracy } from '@ionic-native/location-accuracy';

@Component({
  selector: 'page-view-lead',
  templateUrl: 'view-lead.html',
})
export class ViewLeadPage {

  	user= { user_id:'', lead_id:'', image:'', latLngObj:{} };
	token= { access_token: '', token_type : ''};
	errors:any;
	lead:any;
	google_api_key: any;

	constructor(
		public navCtrl: NavController, 
		public navParams: NavParams,
		public storage: Storage,
		public apiProvider: ApiProvider,
		public spinner: SpinnerProvider,
		public modalCtrl : ModalController,
		public alertCtrl: AlertController,
		private callNumber: CallNumber,
		public actionSheetCtrl: ActionSheetController,
		private camera: Camera,
		public geolocation: Geolocation,
        private androidPermissions: AndroidPermissions,
        private locationAccuracy: LocationAccuracy
	){
		this.lead = navParams.get('data');
		this.user.lead_id = this.lead.lead_id;
		this.google_api_key = 'AIzaSyCrSdO6eCgjTZQRL2Rc1A3PfHE7VwvhUD8';
		this.getUser();
	}

	getUser() {
		this.storage.get('user').then((data) => {
			if(data != null){
				this.user.user_id = data.user.user_id;
				this.token.access_token = data.access_token;
				this.token.token_type = data.token_type;
			}
		});
	}

	makeCall() {
		this.callNumber.callNumber(this.lead.mobile_no, true)
		  .then(res => console.log('Launched dialer!', res))
		  .catch(err => console.log('Error launching dialer', err));
	}

	changeStatus() {
		let modal = this.modalCtrl.create(LeadStatusPage, { data: this.lead }, { cssClass:"status-modal"});
		modal.onDidDismiss(data => {
			if(data!="close"){
				this.lead = data;
			}
		});
		modal.present();
	}

	comments() {
		this.navCtrl.push(LeadCommentsPage, {
			data : this.lead
		});
	}

	reminder() {
		let modal = this.modalCtrl.create(LeadReminderPage, { data: this.lead }, { cssClass:"status-modal"});
		modal.onDidDismiss(data => {
			if(data!="close"){
				this.lead = data;
			}
		});
		modal.present();
	}

	image() {
		let actionSheet = this.actionSheetCtrl.create({
	     	title: 'Capture Image',
	    	buttons: [{
	         	text: 'Use Camera',
	         	handler: () => {
	           		this.checkGPSPermission();
	         	}
	       	},
	       	{
	         	text: 'Cancel',
	         	role: 'cancel',
	         	handler: () => {
	           		console.log('Cancel clicked');
	         	}
	       	}]
	   	});
	   	actionSheet.present();
	}

	takePicture() {
		const options: CameraOptions = {
		  	quality: 100,
		  	destinationType: this.camera.DestinationType.DATA_URL,
		  	encodingType: this.camera.EncodingType.PNG,
		  	mediaType: this.camera.MediaType.PICTURE
		}

		this.camera.getPicture(options).then((imageData) => {
		 	let base64Image = 'data:image/jpeg;base64,' + imageData;
		 	this.user.image = base64Image;
		 	this.saveImage()
		}, error => {
			console.log('Error in getting image' + error);
		});
	}

	saveImage() {
		this.spinner.load();
		this.apiProvider.getData('save_image',this.token,this.user).then((response) => {
			this.spinner.dismiss();
			this.lead = response,
			this.spinner.message('Captured image is successfully uploaded');
		}, 
		(error) => {
			this.spinner.dismiss();
			this.errors = error.error.errors;
			this.spinner.message(error.error.message);
		});
	}

	checkGPSPermission() {
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
          result => {
            if (result.hasPermission) {

              //If having permission show 'Turn On GPS' dialogue
              this.askToTurnOnGPS();
            } else {

              //If not having permission ask for permission
              this.requestGPSPermission();
            }
          },
          err => {
            alert('Please enable location to continue');
          }
        );
    }

    requestGPSPermission() {
        this.locationAccuracy.canRequest().then((canRequest: boolean) => {
          if (canRequest) {
            console.log("4");
          } else {
            //Show 'GPS Permission Request' dialogue
            this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
              .then(
                () => {
                  // call method to turn on GPS
                  this.askToTurnOnGPS();
                },
                error => {
                  //Show alert if user click on 'No Thanks'
                  alert('Please enable location to continue');
                }
              );
          }
        });
    }

    askToTurnOnGPS() {
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
          () => {
            // When GPS Turned ON call method to get Accurate location coordinates
            this.currentLocation()
          },
          error => alert('Please enable location to continue')
        );
      }


	currentLocation() {
        this.geolocation.getCurrentPosition({ maximumAge: 3000, timeout: 5000}).then((position) => {
            let latLngObj = {
                'latitude': position.coords.latitude, 
                'longitude': position.coords.longitude
            };
            this.user.latLngObj = latLngObj;
            this.takePicture()
            
        }, 
        (err) => {
            console.log(JSON.stringify(err));
            this.setAddress("Error in getting the address");
            let latLngObj = {
                'latitude': '', 
                'longitude': ''
            };
            this.user.latLngObj = latLngObj;
            this.takePicture()
        });
    }

    setAddress(message: string) {
        // this.spinner.dismiss();
        alert('Please enable location to continue');
    }

}
