import { Component } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';

import { Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ApiProvider } from '../../providers/api/api';
import { SpinnerProvider } from '../../providers/spinner/spinner';
import { OneSignal } from '@ionic-native/onesignal';

import { HomePage } from '../home/home';

@Component({
	selector: 'page-login',
	templateUrl: 'login.html',
})
export class LoginPage {

	user = { email: '', password : '' };
    player = { user_id: '', player_id: '', push_token: '' };
    token : any;

	constructor(
		public navCtrl: NavController, 
		public navParams: NavParams,
		public menu: MenuController,
		public apiProvider: ApiProvider,
        public spinner: SpinnerProvider,
        public storage: Storage,
        public events: Events,
        private oneSignal: OneSignal,
	){
		this.menu = menu;
        this.menu.enable(false, 'menu')
	}

	login() {   
        this.spinner.load();
        this.apiProvider.login(this.user).then((response) => {
            this.spinner.dismiss();
            this.token = response;
            this.player.user_id = this.token.user.user_id;
            this.storage.set('user',response);
            this.events.publish('user:created', response);
            this.getPlayer();
        }, 
        (error) => {
            this.spinner.dismiss();
            this.spinner.message(error.error.result);
        });
    }

    getPlayer() {
        this.oneSignal.startInit('7412fde6-3dea-48f9-99eb-84d110a56e32', '557251828379');
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
        this.oneSignal.getIds().then((response) => {
            this.player.player_id = response.userId;
            this.player.push_token = response.pushToken;
            this.savePlayer();
        });
        this.oneSignal.endInit();
    }

    savePlayer() {
        this.apiProvider.getData('save_player',this.token,this.player).then((response) => {
            this.navCtrl.setRoot(HomePage);
        }, 
        (error) => {
            this.spinner.message(error.error.message);
        });
    }
}
