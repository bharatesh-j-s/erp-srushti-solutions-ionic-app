import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { SpinnerProvider } from '../../providers/spinner/spinner';
import { ApiProvider } from '../../providers/api/api';

import { NewLeadPage } from '../new-lead/new-lead';
import { ViewLeadPage } from '../view-lead/view-lead';

@Component({
  selector: 'page-leads',
  templateUrl: 'leads.html',
})
export class LeadsPage {
	user= { user_id:'', role:'',};
	token= { access_token: '', token_type : ''};
    leads : any;
    search_leads : any;

    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams,
        public storage: Storage,
        public alertCtrl: AlertController,
        public spinner: SpinnerProvider,
        public apiProvider: ApiProvider,
        public menu: MenuController,
    ){
        this.menu = menu;
        this.menu.enable(true, 'menu');
    }

    ionViewDidEnter() {
		this.storage.get('user').then((data) => {
			if(data != null){
				this.user.user_id = data.user.user_id;
				this.user.role = data.user.role;
				this.token.access_token = data.access_token;
				this.token.token_type = data.token_type;
				this.getLeads();
			}
		});
	}

    getLeads() {
		this.spinner.load();
		this.apiProvider.getData('get_leads',this.token,this.user).then((response) => {
			this.spinner.dismiss();
			this.leads = response;
			this.search_leads = this.leads;
		}, 
		(error) => {
			this.spinner.dismiss();
			this.spinner.message(error.error.message);
		});
	}

	searchLeads(ev: any) {
      	var val = ev.target.value;
      	if (val && val.trim() != '') {
          	this.leads = this.search_leads.filter(lead => {
            	return (lead.lead_name.toLowerCase().indexOf(val.toLowerCase()) > -1) ||
            		   (lead.lead_code.toLowerCase().indexOf(val.toLowerCase()) > -1) ||
            		   (lead.mobile_no.toLowerCase().indexOf(val.toLowerCase()) > -1);
          	})
      	} 
      	else {
          	this.leads = this.search_leads;
        }
    }

    newLead() {
    	this.navCtrl.push(NewLeadPage);
    }

    selectLead(lead) {
    	this.navCtrl.push(ViewLeadPage, {
			data : lead
		});
    }
}
