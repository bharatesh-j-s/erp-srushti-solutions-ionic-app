import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, } from 'ionic-angular';
import { SpinnerProvider } from '../../providers/spinner/spinner';

@Component({
	selector: 'page-offline',
	templateUrl: 'offline.html',
})
export class OfflinePage {
	constructor(
		public navCtrl: NavController, 
		public navParams: NavParams,
		public spinner: SpinnerProvider,
		public menu: MenuController

	){
		this.menu = menu;
		this.menu.enable(false, 'menu');
	}
}
