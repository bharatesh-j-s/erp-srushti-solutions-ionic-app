import { Component } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import { SpinnerProvider } from '../../providers/spinner/spinner';
import { ApiProvider } from '../../providers/api/api';

@Component({
  selector: 'page-lead-comments',
  templateUrl: 'lead-comments.html',
})
export class LeadCommentsPage {

	query= { user_id:'', role:'', lead_id:'', query:'' }
	token= { access_token: '', token_type : ''};
	lead: any;
	errors:any;

	constructor(
  		public navCtrl: NavController, 
		public navParams: NavParams,
		public menu: MenuController,
		public storage: Storage,
		public apiProvider: ApiProvider,
		public spinner: SpinnerProvider,
  	){
  		this.menu = menu;
        this.menu.enable(true, 'menu');
  		this.lead = navParams.get('data');
  		this.query.lead_id = this.lead.lead_id;
  		this.getUser();
  	}

  	getUser() {
		this.storage.get('user').then((data) => {
			if(data != null){
				this.query.user_id = data.user.user_id;
				this.query.role = data.user.role;
				this.token.access_token = data.access_token;
				this.token.token_type = data.token_type;
			}
		});
	}

	saveQuery(){
		this.spinner.load();
		this.apiProvider.getData('save_lead_query',this.token,this.query).then((response) => {
			this.spinner.dismiss();
			this.query.query="";
			this.lead = response;
		}, 
		(error) => {
			this.spinner.dismiss();
			this.errors = error.error.errors;
			this.spinner.message(error.error.message);
		});
	}

}
