import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, Platform } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { Events } from 'ionic-angular';

import { SpinnerProvider } from '../../providers/spinner/spinner';
import { ApiProvider } from '../../providers/api/api';

import { CallsPage } from '../calls/calls';

import { AttendancePage } from '../attendance/attendance';

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {
	user= { user_id:'', role:'',};
	token= { access_token: '', token_type : ''};
	call_types: any;
	unregisterBackButtonAction;
	back = 0;
	status:any;	

  	constructor(
		public navCtrl: NavController, 
		public navParams: NavParams,
		public menu: MenuController,
		public storage: Storage,
		public events: Events,
		public apiProvider: ApiProvider,
		public spinner: SpinnerProvider,
		public platform: Platform,
	){
		this.menu = menu;
        this.menu.enable(true, 'menu');
        this.getUser();
	}

	ionViewDidEnter() {
    	this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
    		if(this.back==0) {
	        	this.spinner.message("Please click back again to exit the app.");
	        }
	        else
	        {
	        	this.platform.exitApp();
	        }
	        this.back = 1;
	    });
	}

	ionViewWillLeave() {
	    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
	}

	getUser() {
		this.storage.get('user').then((data) => {
			if(data != null){
				this.user.user_id = data.user.user_id;
				this.user.role = data.user.role;
				this.token.access_token = data.access_token;
				this.token.token_type = data.token_type;
				this.events.publish('user:created', data);
				this.getAttendanceStatus();
			}
		});
	}

	getAttendanceStatus() {
		this.apiProvider.getData('get_attendance_status',this.token,this.user).then((response) => {
			this.status = response;
			if(this.status==0 && this.user.role!='Super Admin') {
				this.navCtrl.push(AttendancePage);
			}
			else {
				this.getCallTypes();
			}
		}, 
		(error) => {
			this.spinner.message(error.error.message);
		});
	}

	getCallTypes() {
		this.spinner.load();
		this.apiProvider.getData('get_call_types_with_count',this.token,this.user).then((response) => {
			this.spinner.dismiss();
			this.call_types = response;
			this.events.publish('call_types:updated', this.call_types);
		}, 
		(error) => {
			this.spinner.dismiss();
			this.spinner.message(error.error.message);
		});
	}

	openCall(call_type,status) {
		this.navCtrl.push(CallsPage, {
			data : call_type,
			data1 : status
		});
	}
}
