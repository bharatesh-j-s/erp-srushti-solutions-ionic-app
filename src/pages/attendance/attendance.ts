import { Component } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { SpinnerProvider } from '../../providers/spinner/spinner';
import { ApiProvider } from '../../providers/api/api';

import { Geolocation } from '@ionic-native/geolocation';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { LocationAccuracy } from '@ionic-native/location-accuracy';

import { HomePage } from '../home/home';

// import {
// 			BackgroundGeolocation,
// 			BackgroundGeolocationConfig,
// 			BackgroundGeolocationResponse,
// 			BackgroundGeolocationEvents
// } from "@ionic-native/background-geolocation";

@Component({
		selector: 'page-attendance',
		templateUrl: 'attendance.html',
})
export class AttendancePage {

		token= { access_token: '', token_type : ''};
	attendance = {
		user_id:'', 
		login_date_time:'',
		login_note:'',
		login_lat:'',
		login_long:'',
		login_address:'',
		logout_date_time: '',
		logout_note:'',
		logout_lat:'',
		logout_long:'',
		logout_address:'',
	};
	google_api_key: any;
	status:any;	
	errors:any;

	constructor(
			public navCtrl: NavController, 
			public navParams: NavParams,
			public menu: MenuController,
			public storage: Storage,
			public apiProvider: ApiProvider,
			public spinner: SpinnerProvider,
			public geolocation: Geolocation,
			public http: Http,
			// private backgroundGeolocation: BackgroundGeolocation,
			private androidPermissions: AndroidPermissions,
			private locationAccuracy: LocationAccuracy
		){
			this.menu = menu;
				this.menu.enable(true, 'menu');
			this.google_api_key = 'AIzaSyCrSdO6eCgjTZQRL2Rc1A3PfHE7VwvhUD8';
				this.getUser();
		}

		getUser() {
		this.storage.get('user').then((data) => {
			if(data != null){
				this.token.access_token = data.access_token;
				this.token.token_type = data.token_type;
				this.attendance.user_id = data.user.user_id;
				this.getAttendanceStatus();
			}
		});
	}

	getAttendanceStatus() {
		this.spinner.load();
		this.apiProvider.getData('get_attendance_status',this.token,this.attendance).then((response) => {
			this.spinner.dismiss();
			this.status = response;
			this.checkGPSPermission();
		}, 
		(error) => {
			this.spinner.dismiss();
			this.spinner.message(error.error.message);
		});
	}

		checkGPSPermission() {
				this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
					result => {
						if (result.hasPermission) {

							//If having permission show 'Turn On GPS' dialogue
							this.askToTurnOnGPS();
						} else {

							//If not having permission ask for permission
							this.requestGPSPermission();
						}
					},
					err => {
						alert('Please enable location to continue');
					}
				);
		}

		requestGPSPermission() {
				this.locationAccuracy.canRequest().then((canRequest: boolean) => {
					if (canRequest) {
						console.log("4");
					} else {
						//Show 'GPS Permission Request' dialogue
						this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
							.then(
								() => {
									// call method to turn on GPS
									this.askToTurnOnGPS();
								},
								error => {
									//Show alert if user click on 'No Thanks'
									alert('Please enable location to continue');
								}
							);
					}
				});
		}

		askToTurnOnGPS() {
				this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
					() => {
						// When GPS Turned ON call method to get Accurate location coordinates
						this.currentLocation()
					},
					error => alert('Please enable location to continue')
				);
			}


	currentLocation() {
				this.geolocation.getCurrentPosition({ maximumAge: 3000, timeout: 5000}).then((position) => {
						let latLngObj = {
								'lat': position.coords.latitude, 
								'long': position.coords.longitude
						};
						console.log(latLngObj);
						this.getAddress(latLngObj);
				}, 
				(err) => {
						console.log("error: " + err);
						this.setAddress("Error in getting the address");
				});
		}

		getAddress(latLngObj) {
				this.getStreetAddress(latLngObj).subscribe(
						s_address => {
								if(s_address.status == "ZERO_RESULTS"){
										this.getNormalAddress(latLngObj).subscribe(
												address => {
														if(address.status == "ZERO_RESULTS"){
																console.log('No Address found');
																this.setAddress("Error in getting the address");
														}
														else {
																this.getAddressComponentByPlace(address.results[0], latLngObj);
																console.log("Normal Address : "+this.attendance.login_address);
														}
												},
												err => {
														console.log("Error in getting the street address " + err);
														this.setAddress("Error in getting the address");
												}
										)
								} 
								else {
										this.getAddressComponentByPlace(s_address.results[0], latLngObj);
										console.log("Streat Address : "+this.attendance.login_address);
								}
						},
						err => {
								console.log('No Address found ' + err);
								this.setAddress("Error in getting the address");
						}
				);
		}

		getAddressComponentByPlace(place, latLngObj) {
				var components;

				components = {};

				for(var i = 0; i < place.address_components.length; i++){
						let ac = place.address_components[i];
						components[ac.types[0]] = ac.long_name;
				}

				this.attendance.login_address = place.formatted_address;
				this.attendance.login_lat = latLngObj.lat;
				this.attendance.login_long = latLngObj.long;
				this.attendance.logout_address = place.formatted_address;
				this.attendance.logout_lat = latLngObj.lat;
				this.attendance.logout_long = latLngObj.long;
				this.setAddress(this.attendance.login_address)
		}

		getNormalAddress(params) {
				let url = 'https://maps.googleapis.com/maps/api/geocode/json?key='+ this.google_api_key +'&latlng=' + params.lat + ',' + params.long;
				return this.GET(url);
		}

		getStreetAddress(params) {
				let url = 'https://maps.googleapis.com/maps/api/geocode/json?key=' + this.google_api_key + '&latlng=' + params.lat + ',' + params.long + '&result_type=street_address';
				return this.GET(url);
		}

		GET(url) {
				return this.http.get(url).map(res => res.json());
		}

		setAddress(message: string) {
				// this.spinner.dismiss();
				console.log(message);
		}

	login() {
				if(this.attendance.login_lat!='') {
				this.spinner.load();
				this.apiProvider.getData('attendance',this.token,this.attendance).then((response) => {
					this.spinner.dismiss();
					this.status = response;
					this.spinner.message('Attendance status has been Successfully Updated !...');
								// this.backgroundLocation();
								this.navCtrl.push(HomePage);
				}, 
				(error) => {
					this.spinner.dismiss();
					this.errors = error.error.errors;
				});
				}
				else {
						alert('Please enable location to continue')
				}
	}

		// backgroundLocation() {
		// 		if(this.status==1) {
		// 				const config: BackgroundGeolocationConfig = {
		// 						desiredAccuracy: 10,
		// 						stationaryRadius: 20,
		// 						distanceFilter: 30,
		// 						debug: false,
		// 						stopOnTerminate: false,
		// 						startOnBoot:true,
		// 						notificationsEnabled:true,
		// 						interval: 20000,
		// 						notificationTitle:'Business Monitering System',
		// 						notificationText:'Notification System is enabled',
		// 						notificationIconColor:'#f53d3d',
		// 						notificationIconLarge:'favicon.ico',
		// 						notificationIconSmall:'favicon.ico',
		// 						url:this.apiProvider.locationUrl+this.attendance.user_id,
		// 						syncUrl:this.apiProvider.locationUrl+this.attendance.user_id,
		// 				};
		// 				this.backgroundGeolocation.configure(config).then(() => {
		// 						this.backgroundGeolocation.on(BackgroundGeolocationEvents.location).subscribe((location: BackgroundGeolocationResponse) => {
		// 							console.log(location);
		// 							this.backgroundGeolocation.finish();
		// 						});
		// 				});
		// 				this.backgroundGeolocation.start();
		// 		}
		// 		else {
		// 				this.backgroundGeolocation.stop();
		// 		}
		// }
}
