import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { ApiProvider } from '../../providers/api/api';
import { SpinnerProvider } from '../../providers/spinner/spinner';

@Component({
  	selector: 'page-change-status',
  	templateUrl: 'change-status.html',
})
export class ChangeStatusPage {

  	user= { user_id:'', role:'', call_id:'', starus:{}, status_id:'', comment:'' };
	token= { access_token: '', token_type : ''};
	statuses:any;
	errors:any;

  	constructor(
  		public navCtrl: NavController, 
  		public navParams: NavParams,
  		public storage: Storage,
  		public viewCtrl: ViewController, 
		public apiProvider: ApiProvider,
		public spinner: SpinnerProvider,
  	){
  		this.user = navParams.get('data');
  		this.getUser()
  	}

  	getUser() {
		this.storage.get('user').then((data) => {
			if(data != null){
				this.token.access_token = data.access_token;
				this.token.token_type = data.token_type;
				this.getStatuses();
			}
		});
	}

	getStatuses() {
		this.spinner.load();
		this.apiProvider.getData('get_statuses',this.token,this.user).then((response) => {
			this.spinner.dismiss();
			this.statuses = response;
		}, 
		(error) => {
			this.spinner.dismiss();
			this.spinner.message(error.error.message);
		});
	}

	dismiss() {
        this.viewCtrl.dismiss('close');
    }

    changeStatus() {
    	this.spinner.load();
		this.apiProvider.getData('change_status',this.token,this.user).then((response) => {
			this.spinner.dismiss();
			this.user.comment="";
			this.viewCtrl.dismiss(response);
		}, 
		(error) => {
			this.spinner.dismiss();
			this.errors = error.error.errors;
			this.spinner.message(error.error.message);
		});
    }

}
