import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, ModalController } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import { SpinnerProvider } from '../../providers/spinner/spinner';
import { ApiProvider } from '../../providers/api/api';

import { CallNumber } from '@ionic-native/call-number';

import { CommentsPage } from '../comments/comments';
import { ChangeStatusPage } from '../change-status/change-status';

@Component({
  selector: 'page-view-call',
  templateUrl: 'view-call.html',
})
export class ViewCallPage {

	user= { user_id:'', role:'', call_id:'', starus:{}, status_id:'', comment:'' };
	token= { access_token: '', token_type : ''};
	call: any;

	constructor(
  		public navCtrl: NavController, 
		public navParams: NavParams,
		public menu: MenuController,
		public modalCtrl : ModalController,
		public storage: Storage,
		public apiProvider: ApiProvider,
		public spinner: SpinnerProvider,
		private callNumber: CallNumber,
  	){
  		this.menu = menu;
        this.menu.enable(true, 'menu');
  		this.call = navParams.get('data');
  		this.user.call_id = this.call.call_id;
  		this.user.status_id = this.call.status_id;
        this.getUser();
  	}

  	getUser() {
		this.storage.get('user').then((data) => {
			if(data != null){
				this.user.user_id = data.user.user_id;
				this.user.role = data.user.role;
				this.token.access_token = data.access_token;
				this.token.token_type = data.token_type;
			}
		});
	}

	getCall() {
		this.spinner.load();
		this.apiProvider.getData('get_call',this.token,this.user).then((response) => {
			this.spinner.dismiss();
			this.call = response;
			this.call.status = this.call.status;
		}, 
		(error) => {
			this.spinner.dismiss();
			this.spinner.message(error.error.message);
		});
	}

	makeCall() {
		this.callNumber.callNumber(this.call.customer.mobile_no, true)
		  .then(res => console.log('Launched dialer!', res))
		  .catch(err => console.log('Error launching dialer', err));
	}

	comments() {
		this.navCtrl.push(CommentsPage, {
			data : this.call
		});
	}

	changeStatus() {
		let modal = this.modalCtrl.create(ChangeStatusPage, { data: this.user }, { cssClass:"status-modal"});
		modal.onDidDismiss(data => {
			if(data!="close"){
				this.getCall();
			}
		});
		modal.present();
	}
}
