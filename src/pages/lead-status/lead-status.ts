import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { ApiProvider } from '../../providers/api/api';
import { SpinnerProvider } from '../../providers/spinner/spinner';

@Component({
  selector: 'page-lead-status',
  templateUrl: 'lead-status.html',
})
export class LeadStatusPage {

	user= { user_id:'', lead_id:'', status:'', comment:'' };
	lead:any;
	token= { access_token: '', token_type : ''};
	statuses:any;
	errors:any;

  	constructor(
  		public navCtrl: NavController, 
  		public navParams: NavParams,
  		public storage: Storage,
  		public viewCtrl: ViewController, 
		public apiProvider: ApiProvider,
		public spinner: SpinnerProvider,
  	){
  		this.lead = navParams.get('data');
  		this.user.lead_id = this.lead.lead_id;
  		this.user.status = this.lead.status;
  		this.getUser()
  	}

  	getUser() {
		this.storage.get('user').then((data) => {
			if(data != null){
				this.user.user_id = data.user.user_id;
				this.token.access_token = data.access_token;
				this.token.token_type = data.token_type;
			}
		});
	}

	dismiss() {
        this.viewCtrl.dismiss('close');
    }

    changeStatus() {
    	this.spinner.load();
		this.apiProvider.getData('change_lead_status',this.token,this.user).then((response) => {
			this.spinner.dismiss();
			this.user.comment="";
			this.viewCtrl.dismiss(response);
		}, 
		(error) => {
			this.spinner.dismiss();
			this.errors = error.error.errors;
			this.spinner.message(error.error.message);
		});
    }

}
