import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { SpinnerProvider } from '../../providers/spinner/spinner';
import { ApiProvider } from '../../providers/api/api';

import { ViewLeadPage } from '../view-lead/view-lead';

@Component({
  selector: 'page-new-lead',
  templateUrl: 'new-lead.html',
})
export class NewLeadPage {
	token= { access_token: '', token_type : ''};
    lead= { 
		user_id:'', 
		lead_code:'',
		lead_name:'',
		lead_type_id:'',
		category_id:'',
		mobile_no:'',
		alternate_no:'',
		email:'',
		reference_id:'',
		area_id:'',
		address:'',
	};
	lead_types:any;
	categories:any;
	references:any;
	areas:any;
	errors:any;

    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams,
        public storage: Storage,
        public alertCtrl: AlertController,
        public spinner: SpinnerProvider,
        public apiProvider: ApiProvider,
        public menu: MenuController,
    ){
        this.menu = menu;
        this.menu.enable(true, 'menu');
        this.getUser();
    }

    getUser() {
		this.storage.get('user').then((data) => {
			if(data != null){
				this.lead.user_id = data.user.user_id;
				this.token.access_token = data.access_token;
				this.token.token_type = data.token_type;
				this.getLeadInfo();
			}
		});
	}

	getLeadInfo() {
		this.spinner.load();
		this.apiProvider.getData('get_lead_info',this.token,this.lead).then((response) => {
			this.spinner.dismiss();
			this.lead.lead_code = response['lead_code'];
			this.lead_types = response['lead_types'];
			this.categories = response['categories'];
			this.references = response['references'];
			this.areas = response['areas'];
		}, 
		(error) => {
			this.spinner.dismiss();
			this.spinner.message(error.error.message);
		});
	}

	save() {
		this.spinner.load();
		this.apiProvider.getData('save_lead',this.token,this.lead).then((response) => {
			this.spinner.dismiss();
			this.navCtrl.push(ViewLeadPage, {
				data : response
			});
		}, 
		(error) => {
			this.spinner.dismiss();
			this.errors = error.error.errors;
			this.spinner.message(error.error.message);
		});
	}
}
