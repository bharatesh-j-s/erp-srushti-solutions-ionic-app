import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Events } from 'ionic-angular';

import { SpinnerProvider } from '../../providers/spinner/spinner';
import { ApiProvider } from '../../providers/api/api';

import { HomePage } from '../home/home';

@Component({
  	selector: 'page-profile',
  	templateUrl: 'profile.html',
})
export class ProfilePage {

	token= { access_token: '', token_type : ''};
	user = {
		user_id:'',
		name:'',
		email:'',
		mobile_no:'',
		avatar:'',
	};
	errors:any;

  	constructor(
  		public navCtrl: NavController, 
  		public navParams: NavParams,
  		public storage: Storage,
  		public events: Events,
		public apiProvider: ApiProvider,
		public spinner: SpinnerProvider,
  	){
  		this.getUser();
  	}

  	getUser() {
		this.storage.get('user').then((data) => {
			if(data != null){
				this.token.access_token = data.access_token;
				this.token.token_type = data.token_type;
				this.user.user_id = data.user.user_id;
				this.user.name = data.user.name;
				this.user.email = data.user.email;
				this.user.mobile_no = data.user.mobile_no;
				this.user.avatar = data.user.avatar;
			}
		});
	}

	update() {
		this.spinner.load();
		this.apiProvider.getData('update_profile',this.token,this.user).then((response) => {
			this.spinner.dismiss();
			this.spinner.message('Profile Successfully Updated !...');
			this.storage.set('user',response);
            this.events.publish('user:created', response);
            this.navCtrl.setRoot(HomePage);
		}, 
		(error) => {
			this.spinner.dismiss();
			this.errors = error.error.errors;
		});
	}

}
