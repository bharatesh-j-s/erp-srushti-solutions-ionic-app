import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { ApiProvider } from '../../providers/api/api';
import { SpinnerProvider } from '../../providers/spinner/spinner';

@Component({
  	selector: 'page-filter-calls',
  	templateUrl: 'filter-calls.html',
})
export class FilterCallsPage {

	filter= { user_id:'', role:'', call_type_id:'', from_date:'', to_date:'', category_id:'', status_id:''};
	token= { access_token: '', token_type : ''};
	categories: any;
	statuses:any;

  	constructor(
  		public navCtrl: NavController, 
  		public navParams: NavParams,
  		public storage: Storage,
  		public viewCtrl: ViewController, 
		public apiProvider: ApiProvider,
		public spinner: SpinnerProvider,
  	){
  		this.filter = navParams.get('data');
  		this.getUser()
  	}

  	getUser() {
		this.storage.get('user').then((data) => {
			if(data != null){
				this.token.access_token = data.access_token;
				this.token.token_type = data.token_type;
				this.getCategories();
			}
		});
	}

	dismiss() {
        this.viewCtrl.dismiss('close');
    }

	getCategories() {
		this.spinner.load();
		this.apiProvider.getData('get_categories',this.token,this.filter).then((response) => {
			this.categories = response;
			this.getStatuses();
		}, 
		(error) => {
			this.spinner.dismiss();
			this.spinner.message(error.error.message);
		});
	}

	getStatuses() {
		this.apiProvider.getData('get_statuses',this.token,this.filter).then((response) => {
			this.spinner.dismiss();
			this.statuses = response;
		}, 
		(error) => {
			this.spinner.dismiss();
			this.spinner.message(error.error.message);
		});
	}

	filterCalls() {
		this.viewCtrl.dismiss(this.filter);
	}
}
