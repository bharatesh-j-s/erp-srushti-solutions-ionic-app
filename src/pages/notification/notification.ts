import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { SpinnerProvider } from '../../providers/spinner/spinner';
import { ApiProvider } from '../../providers/api/api';

@Component({
    selector: 'page-notification',
    templateUrl: 'notification.html',
})
export class NotificationPage {
    user= { user_id:'', role:'',};
	token= { access_token: '', token_type : ''};
    notifications : any;

    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams,
        public storage: Storage,
        public alertCtrl: AlertController,
        public spinner: SpinnerProvider,
        public apiProvider: ApiProvider,
        public menu: MenuController,
    ){
        this.menu = menu;
        this.menu.enable(true, 'menu');
        this.getUser();
    }

    getUser() {
		this.storage.get('user').then((data) => {
			if(data != null){
				this.user.user_id = data.user.user_id;
				this.user.role = data.user.role;
				this.token.access_token = data.access_token;
				this.token.token_type = data.token_type;
				this.getNotifications();
			}
		});
	}

    getNotifications() {
		this.spinner.load();
		this.apiProvider.getData('get_notifications',this.token,this.user).then((response) => {
			this.spinner.dismiss();
			this.notifications = response;
		}, 
		(error) => {
			this.spinner.dismiss();
			this.spinner.message(error.error.message);
		});
	}

	clearNotifications() {
		this.spinner.load();
		this.apiProvider.getData('clean_notifications',this.token,this.user).then((response) => {
			this.spinner.dismiss();
			this.notifications = response;
		}, 
		(error) => {
			this.spinner.dismiss();
			this.spinner.message(error.error.message);
		});
	}

    showNotification(notification){
        let alert = this.alertCtrl.create({
            title: notification.subject,
            message: notification.message,
            buttons: ['Ok']
        });
        alert.present()
    }
}
