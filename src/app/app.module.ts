import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { ProfilePage } from '../pages/profile/profile';
import { CallsPage } from '../pages/calls/calls';
import { FilterCallsPage } from '../pages/filter-calls/filter-calls';
import { ViewCallPage } from '../pages/view-call/view-call';
import { CommentsPage } from '../pages/comments/comments';
import { ChangeStatusPage } from '../pages/change-status/change-status';
import { AttendancePage } from '../pages/attendance/attendance';
import { NotificationPage } from '../pages/notification/notification';
import { OfflinePage } from '../pages/offline/offline';

import { LeadsPage } from '../pages/leads/leads';
import { NewLeadPage } from '../pages/new-lead/new-lead';
import { ViewLeadPage } from '../pages/view-lead/view-lead';
import { LeadStatusPage } from '../pages/lead-status/lead-status';
import { LeadCommentsPage } from '../pages/lead-comments/lead-comments';
import { LeadReminderPage } from '../pages/lead-reminder/lead-reminder';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CallNumber } from '@ionic-native/call-number';
import { Market } from '@ionic-native/market';
import { Network } from '@ionic-native/network';
import { OneSignal } from '@ionic-native/onesignal';
import { Toast } from '@ionic-native/toast';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Geolocation } from '@ionic-native/geolocation';
// import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Camera } from '@ionic-native/camera';

import { ApiProvider } from '../providers/api/api';
import { SpinnerProvider } from '../providers/spinner/spinner';

@NgModule({
	declarations: [
		MyApp,
		LoginPage,
		HomePage,
		ProfilePage,
		CallsPage,
		FilterCallsPage,
		ViewCallPage,
		CommentsPage,
		ChangeStatusPage,
		AttendancePage,
		NotificationPage,
		OfflinePage,
		LeadsPage,
		NewLeadPage,
		ViewLeadPage,
		LeadStatusPage,
		LeadCommentsPage,
		LeadReminderPage,
	],
	imports: [
		BrowserModule,
		HttpModule,
		HttpClientModule,
		IonicStorageModule.forRoot(),
		IonicModule.forRoot(MyApp),
	],
	bootstrap: [IonicApp],
	entryComponents: [
		MyApp,
		LoginPage,
		HomePage,
		ProfilePage,
		CallsPage,
		FilterCallsPage,
		ViewCallPage,
		CommentsPage,
		ChangeStatusPage,
		AttendancePage,
		NotificationPage,
		OfflinePage,
		LeadsPage,
		NewLeadPage,
		ViewLeadPage,
		LeadStatusPage,
		LeadCommentsPage,
		LeadReminderPage,
	],
	providers: [
		StatusBar,
		SplashScreen,
		{provide: ErrorHandler, useClass: IonicErrorHandler},
		ApiProvider,
		SpinnerProvider,
		CallNumber,
		Market,
		Network,
		OneSignal,
		Toast,
		Geolocation,
		AndroidPermissions,
		LocationAccuracy,
		// BackgroundGeolocation,
		Camera
	]
})
export class AppModule {}
