import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, AlertController } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Market } from '@ionic-native/market';
import { Network } from '@ionic-native/network';
import { OneSignal } from '@ionic-native/onesignal';

import { Storage } from '@ionic/storage';
import { Events } from 'ionic-angular';

import { SpinnerProvider } from '../providers/spinner/spinner';
import { ApiProvider } from '../providers/api/api';

import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { ProfilePage } from '../pages/profile/profile';
import { CallsPage } from '../pages/calls/calls';
import { AttendancePage } from '../pages/attendance/attendance';
import { NotificationPage } from '../pages/notification/notification';
import { OfflinePage } from '../pages/offline/offline';
import { LeadsPage } from '../pages/leads/leads';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  user = { user_id:'', name: '', email: '', mobile_no: '', avatar: '', role:'' };
  token = { access_token: '', token_type : ''};
  version = { android:'0.0.3', ios: '0.0.1' };
  call_types:any;

  constructor(
    public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen,
    public storage: Storage,
    public events: Events,
    public alertCtrl: AlertController,
    public apiProvider: ApiProvider,
    public spinner: SpinnerProvider,
    private market: Market,
    private network: Network,
    private oneSignal: OneSignal
  ){
    this.storage.get('user').then((data) => {
      if(data == null){
        this.rootPage = LoginPage;
      }
      else{
        this.rootPage = HomePage;
      }
      events.subscribe('call_types:updated', (data) => {
        this.call_types = data;
      });
      events.subscribe('user:created', (data) => {
        this.user.user_id = data.user.user_id;
        this.user.name = data.user.name;
        this.user.email = data.user.email;
        this.user.avatar = data.user.avatar;
        this.user.role = data.user.role;
        this.token.access_token = data.access_token;
        this.token.token_type = data.token_type;
      });
      this.initializeApp();
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleLightContent();
      this.statusBar.backgroundColorByHexString('#da0b0b');
      this.splashScreen.hide();

      //Check Version
        this.checkVersion();

        //Network
        // this.network.onDisconnect().subscribe(() => {
        //     this.nav.setRoot(OfflinePage);
        // });

        // this.network.onConnect().subscribe(() => {
        //     this.storage.get('user').then((data) => {
        //         if(data == null){
        //             this.nav.setRoot(LoginPage);
        //         }
        //         else{
        //             this.nav.setRoot(HomePage);
        //         }
        //     })
        // });

        //One Signal
        this.oneSignal.startInit('7412fde6-3dea-48f9-99eb-84d110a56e32', '557251828379');
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);

        this.oneSignal.handleNotificationReceived().subscribe(() => {
             // do something when notification is received
        });

        this.oneSignal.handleNotificationOpened().subscribe(() => {
            // do something when a notification is opened
        });
        this.oneSignal.endInit();
    });
  }

  homePage() {
    this.nav.setRoot(HomePage);
  }

  openCall(call_type,status) {
    this.nav.push(CallsPage, {
      data : call_type,
      data1 : status
    });
  }

  attendancePage() {
    this.nav.push(AttendancePage);
  }

  leadsPage() {
    this.nav.push(LeadsPage);
  }

  notificationPage() {
    this.nav.push(NotificationPage);
  }

  profile() {
    this.nav.push(ProfilePage);
  }

  logout(){
        this.storage.remove('user');
        this.nav.setRoot(LoginPage);
    }

    checkVersion()
  {   
    this.apiProvider.checkVersion(this.user).then((response) => {
      if(response['android']!=this.version.android){
        this.presentAlert();
      }
    }, 
    (error) => {
      this.spinner.message(error.error.message);
    });
  }

  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'Kindly update application to use services.',
      subTitle: 'Press Ok to redirect Play Store',
      buttons: [
        {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
            this.market.open('in.srushtisolutions.www');
            this.platform.exitApp();
        }
       },
        {
        text: 'OK',
        handler: () => {
            this.market.open('in.srushtisolutions.www');
            this.platform.exitApp();
        }
        }]
      });
      alert.present();
    }
}
